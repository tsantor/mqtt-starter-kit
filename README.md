# MQTT Starter Kit
Tim Santor <tsantor@xstudios.com>

## Overview
A quick head start on creating a MQTT subscriber and/or publisher client.

## Install
For this example, we're leveraging [Mosquitto](https://mosquitto.org/) and [Paho](https://www.eclipse.org/paho/).

### Mac OS X
```bash
$ brew install mosquitto
$ brew services start mosquitto
```

### Ubuntu/Debian
```bash
$ sudo apt-get install mosquitto mosquitto-clients
```

### Virtualenv
```bash
mkvirtualenv mqtt_env
pip install -r requirements.txt
```

## Running the Publisher/Subscriber
Open a terminal window and run:
```bash
$ python subscriber.py --config=client.cfg
# In another terminal window
$ python publisher.py --config=publisher.cfg
```

> **NOTE:** You can open terminal windows and run as many clients as you want. Ensure you create a config for each client and give each a unique `id`.
