# -*- coding: utf-8 -*-

import argparse
import configparser
import logging
import logging.handlers
import os
import signal

import paho.mqtt.client as mqtt
from core.mqtt import MqttClient

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------

app = None
mqttc = None


class MyMQTTClass(mqtt.Client):

    def on_connect(self, mqttc, userdata, flags, rc):
        logger.info('Connected to {}:{}, userdata={}, rc={}'.format(
            mqttc._host, mqttc._port, userdata, rc))
        # Subscribing in on_connect() means that if we lose the connection
        # and reconnect then subscriptions will be renewed.
        self.subscribe(topic='xstudios/#')

    def on_disconnect(self, mqttc, userdata, rc):
        self.loop_stop()
        logger.info('Disconnected...rc={}'.format(rc))

    def on_subscribe(self, mqttc, userdata, mid, granted_qos):
        logger.info('Subscribed: {}, QoS: {}'.format(mid, granted_qos))

    def on_unsubscribe(self, mqttc, userdata, mid, granted_qos):
        logger.info('Unsubscribed: {}, QoS: {}'.format(mid, granted_qos))

    # def on_log(self, mqttc, userdata, level, string):
    #     logger.info(string)

    def run(self, host, port=1883, keepalive=60):
        self.connect(host, port, keepalive)
        return self.loop_forever()

    def cleanup(self):
        logger.debug("Cleanup - stop loop and disconect")
        self.loop_stop()
        self.disconnect()


def _handle_shutdown_signals(signum, frame):
    assert signum in (signal.SIGINT, signal.SIGTERM)
    # logger.info('signal -%d received', signum)
    logger.info('Received graceful shutdown request')
    # global app
    # app.running = False
    mqttc.disconnect()


def parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description='Run MQTT client.')
    parser.add_argument('-c', '--config', help='config file path',
                        default='client.cfg')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='verbose output')
    return parser.parse_args()


def read_config(file_path):
    """Read config."""
    config_file = os.path.expanduser(file_path)
    config = configparser.ConfigParser()
    # config.optionxform = str  # Pass config options case sensitively
    config.read(config_file)
    return config


def on_message(mqttc, userdata, msg):
    logger.info('-' * 80)
    logger.info('Message received...')
    logger.info('Topic: {topic} \nQoS: {qos} \nMessage:{message}'.format(
        topic=msg.topic,
        qos=msg.qos,
        message=msg.payload
    ))


def main():
    """Run script."""
    global mqttc

    # Things seem to fail more gracefully if we trigger the stop
    # out of band (with a signal handler) instead of catching the
    # KeyboardInterrupt...
    signal.signal(signal.SIGINT, _handle_shutdown_signals)
    signal.signal(signal.SIGTERM, _handle_shutdown_signals)

    # Parse command line args and read config
    args = parse_args()
    config = read_config(args.config)

    # Setup logging
    logging.basicConfig(
        level=logging.INFO,
        format='[%(levelname)s] [%(asctime)s] [%(name)s] %(message)s',
        datefmt='%Y-%m-%d %I:%M:%S %p'
    )
    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    # Connect to MQTT broker
    host = config.get('mqtt', 'host')
    port = config.getint('mqtt', 'port')
    keep_alive = config.getint('mqtt', 'keep_alive')
    mqttc = MyMQTTClass(protocol=mqtt.MQTTv311)
    # mqttc = MqttClient(protocol=mqtt.MQTTv311, topics=['xstudios/#'])
    mqttc.on_message = on_message
    mqttc.user_data_set({'foo': 'bar'})

    mqttc.run(host, port, keep_alive)

    # Shutdown cleanly
    mqttc.cleanup()
    logger.info('Shut down gracefully')


if __name__ == '__main__':
    main()
