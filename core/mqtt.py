# -*- coding: utf-8 -*-

import json
import logging
import os
import signal
import time

import paho.mqtt.client as mqtt

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------


class MqttClient(mqtt.Client):
    """A boilerplate MQTT client that has some basic logging on
    events and runs in the background by default (non-blocking)."""

    def __init__(
        self,
        client_id="",
        clean_session=True,
        userdata=None,
        protocol=mqtt.MQTTv311,
        transport="tcp",
        topics=[],
    ):
        """Override __init__ so we can pass in a topic."""
        self.topics = topics
        super().__init__(client_id, clean_session, userdata, protocol, transport)

    def on_connect(self, mqttc, userdata, flags, rc):
        logger.info(
            f"Connected to {mqttc._host}:{mqttc._port}, flags={flags}, userdata={userdata}, rc={rc}"
        )
        # Subscribing in on_connect() means that if we lose the connection
        # and reconnect then subscriptions will be renewed
        for t in self.topics:
            logger.info(f'Subscribe to: "{t}"')
            self.subscribe(topic=t)

    def on_disconnect(self, mqttc, userdata, rc):
        # If we don't stop the loop it'll try to reconnect automatically
        # self.loop_stop()
        logger.info(f"Disconnected...rc={rc}")
        if rc != 0:
            logger.warning(f"Unexpected disconnection...rc={rc}")

    def on_publish(self, mqttc, userdata, mid):
        logger.info(f"Published: mid={mid}, userdata={userdata}")

    def on_subscribe(self, mqttc, userdata, mid, granted_qos):
        logger.info(
            f"Subscribed: {mid}, granted_qos={granted_qos}, userdata={userdata}"
        )

    def on_unsubscribe(self, mqttc, userdata, mid, granted_qos):
        logger.info(
            f"Unsubscribed: mid={mid}, userdata={userdata}, granted_qos={granted_qos}"
        )

    # def on_log(self, mqttc, userdata, level, string):
    #     logger.info(string)

    def run(self, host, port=1883, keepalive=60, forever=True):
        # logger.info("Run MQTT ...")
        self.connect(host, port, keepalive)
        if forever:
            # Blocking call
            self.loop_forever()
        else:
            # Background thread
            self.loop_start()

    def cleanup(self):
        logger.debug("Cleanup - stop loop and disconect")
        self.loop_stop()
        self.disconnect()
